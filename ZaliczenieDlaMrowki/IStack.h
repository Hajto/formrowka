#pragma once

template <class T>
class IStack
{
public:
	virtual ~IStack() {}
	virtual void push(T elem) = 0;
	virtual T pop() = 0;
	virtual void reverse() = 0;
	virtual int length() = 0;
	virtual bool isEmpty() = 0;
};