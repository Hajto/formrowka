﻿#include "MemoryManager.h"
#include "stdlib.h"
#include <cstring>
#include <cstdio>

#define INITIAL_SIZE 6
#define INCREMENTAL_STEP 3

static MemoryAllocationNode* memoryArray = NULL;

static size_t TotalOccupedMem = 0; //Ogólnie alokowana pamięć, B.
static size_t last = 0; //pierwsza wolna pozycja w MemTab.
static size_t AllocCount = 0;

int findIndex(void* elem);

int MemInit()
{
	memoryArray = (MemoryAllocationNode*) malloc(INITIAL_SIZE * sizeof(MemoryAllocationNode));
	if (!memoryArray) {
		exit(1); // DO some error handling
	}
	memset(memoryArray, 0, INITIAL_SIZE * sizeof(MemoryAllocationNode));

	return 1;
}

void * MemAllocate(size_t how_much)
{
	size_t size = _msize(memoryArray) / sizeof(MemoryAllocationNode);

	if (last >= size) {
		memoryArray = (MemoryAllocationNode*)realloc(memoryArray, (size+INCREMENTAL_STEP)* sizeof(MemoryAllocationNode));
		if (memoryArray)
			memset(memoryArray + size, 0, INCREMENTAL_STEP * sizeof(MemoryAllocationNode));
	}

	memoryArray[last].allocated = malloc(how_much);
	if (memoryArray[last].allocated)
	{
		memoryArray[last].when = AllocCount++;
		TotalOccupedMem += how_much;
	}
	return memoryArray[last++].allocated;
}

void MemFree(void * ptr)
{
	int index = findIndex(ptr);
	if (index >= 0) {
		free(ptr);
		memoryArray[index].allocated = NULL;
	}
}

void CloseManager()
{
	for (int i = 0; i < last; i++) {
		if (memoryArray[i].allocated != NULL) {
			printf("Leak at %zu\n", memoryArray[i].when);
			free(memoryArray[i].allocated);
		}
	}
}

int findIndex(void* elem) {
	for (int i = 0; i < last; i++) {
		if (elem == memoryArray[i].allocated) return i;
	}
	return -1;
}

