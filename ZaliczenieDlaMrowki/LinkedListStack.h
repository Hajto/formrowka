#pragma once
#include "IStack.h"
#include <string>
#include <iostream>

template <typename T> struct Link
{
	T value;
	Link<T>* next = nullptr;
};

template <typename T> 
class LinkedListStack : public IStack<T> {
public:
	LinkedListStack<T>() {}
	~LinkedListStack<T>();

	//IStack members override
	void push(T elem) override;
	T pop() override;
	void reverse() override;
	int length() override;
	bool isEmpty() override;
private:
	int count = 0;
	Link<T>* next = nullptr;
};

class StackAlreadyEmptyException : public std::exception
{
	std::string what_message = "Stack is already empty! Can't pop it more!";
public:
	virtual const char* what() const throw()
	{
		return what_message.c_str();
	}
};