#include "LinkedListStack.h"
#include "MemoryManager.h"

template <typename T>
LinkedListStack<T>::~LinkedListStack()
{
	while (next != nullptr)
	{
		Link<T>* temp = next->next;
		MemFree(next);
		next = temp;
	}
}

template <typename T>
bool LinkedListStack<T>::isEmpty()
{
	return next == nullptr;
}

template <typename T>
void LinkedListStack<T>::push(T elem)
{
	Link<T>* temp = nullptr;
	if (temp = (Link<T>*) MemAllocate(sizeof(Link<T>)))
	{
		temp->value = elem;
		temp->next = nullptr;
		count++;
		if (isEmpty())
		{
			next = temp;
		}
		else {
			next->next = temp;
		}
	}
}

template <typename T>
T LinkedListStack<T>::pop()
{
	if (isEmpty())
	{
		throw StackAlreadyEmptyException();
	}
	T temp = next->value;
	Link<T>* toFree = next;
	next = next->next;
	MemFree(toFree);
	count--;
	return temp;
}

template <typename T>
void LinkedListStack<T>::reverse()
{
	//Won't work, TODO: FIX
	Link<T>* new_head = nullptr;
	new_head = next;
	while (next != nullptr)
	{
		new_head->next = next;
		next = next->next;
	}
	next = new_head;
}

template <typename T>
int LinkedListStack<T>::length()
{
	return count;
}
