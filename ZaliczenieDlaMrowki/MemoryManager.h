#pragma once

struct MemoryAllocationNode {
	void* allocated;
	size_t when;
};

int MemInit();
void* MemAllocate(size_t how_much);
void MemFree(void *ptr);

void CloseManager();
